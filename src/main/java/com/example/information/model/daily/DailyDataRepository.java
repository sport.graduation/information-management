package com.example.information.model.daily;

import com.example.information.model.parcticetime.PracticeData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DailyDataRepository extends JpaRepository<DailyData, Long> {
    DailyData findByPlayerID(long PlayerID);
    List<DailyData> findAllByTeamIDAndStartTimeMillis(long teamID, long startTimeMillis);
    List<DailyData> findByStartTimeMillisAndPlayerIDIn(long startTimeMillis,List<Long> playerID);
    DailyData findTopByOrderByPlayerIDDesc();
    DailyData findByPlayerIDAndStartTimeMillis(long playerID,long startTimeMillis);
}

