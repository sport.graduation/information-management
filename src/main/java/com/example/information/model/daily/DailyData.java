package com.example.information.model.daily;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor

@Table(name = "dailyData")
@Entity
public class DailyData implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dailyData")
    @SequenceGenerator(name="dailyData", sequenceName = "dailyData_seq")
    private long id;
    private long playerID;
    private long teamID;
    private long startTimeMillis;
    private long endTimeMillis;
    private double relaxedHeartRate;
    private double lightIntensityHeartRate;
    private double moderateIntensityHeartRate;
    private double vigorousIntensityHeartRate;
    private double sleepHours;
}
