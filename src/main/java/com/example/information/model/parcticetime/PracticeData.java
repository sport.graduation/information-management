package com.example.information.model.parcticetime;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor


@Table(name = "practiceData")
@Entity
public class PracticeData implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "practiceData")
    @SequenceGenerator(name="practiceData", sequenceName = "practiceData_seq")
    private long id;
    private long playerID;
    private long teamID;
    private long startTimeMillis;
    private long endTimeMillis;
    private int totalDistance;
    private double maxSpeed;
    private int burnedCalories ;
    private int numOfSprints ;
    private double hsr ;
    private int avgHeartRate;
    private int maxHeartRate;
    private int minHeartRate;
    private int redZoneDuration;
}
