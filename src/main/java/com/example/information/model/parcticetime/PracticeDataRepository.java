package com.example.information.model.parcticetime;

import com.example.information.model.daily.DailyData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PracticeDataRepository extends JpaRepository<PracticeData, Long> {
    PracticeData findByPlayerID(long playerID);
    List<PracticeData> findAllByTeamIDAndStartTimeMillis(long teamID,long startTimeMillis);
    List<PracticeData> findByPlayerIDIn(List<Long> iterator);
    PracticeData findTopByOrderByPlayerIDDesc();
    PracticeData findByPlayerIDAndStartTimeMillis(long playerID,long startTimeMillis);
    List<PracticeData> findByStartTimeMillisAndPlayerIDIn(long startTimeMillis, List<Long> playerID);
    List<PracticeData> findByPlayerIDAndStartTimeMillisGreaterThanEqualAndEndTimeMillisLessThanEqual(long playerID,long start, long end);

}
