package com.example.information.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("${practicedata.rabbitmq.queue}")
    private String queuePracticeData;

    @Value("${practicedata.rabbitmq.exchange}")
    private String exchangePracticeData;

    @Value("${practicedata.rabbitmq.routingkey}")
    private String routingkeyPracticeData;

    @Value("${dailydata.rabbitmq.queue}")
    private String queueDailyData;

    @Value("${dailydata.rabbitmq.exchange}")
    private String exchangeDailyData;

    @Value("${dailydata.rabbitmq.routingkey}")
    private String routingKeyDailyData;

    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    @Value("${spring.rabbitmq.host}")
    private String host;

    @Bean
    Queue queuePracticeData() {
        return new Queue(queuePracticeData, true);
    }

    @Bean
    Queue queueDailyData() {
        return new Queue(queueDailyData, true);
    }

    @Bean
    Exchange exchangePracticeData() {
        return ExchangeBuilder.directExchange(exchangePracticeData).durable(true).build();
    }

    @Bean
    Exchange exchangeDailyData() {
        return ExchangeBuilder.directExchange(exchangeDailyData).durable(true).build();
    }

    @Bean
    Binding bindingPracticeData() {
        return BindingBuilder
                .bind(queuePracticeData())
                .to(exchangePracticeData())
                .with(routingkeyPracticeData)
                .noargs();
    }
    @Bean
    Binding bindingDailyData() {
        return BindingBuilder
                .bind(queueDailyData())
                .to(exchangeDailyData())
                .with(routingKeyDailyData)
                .noargs();
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(host);
        cachingConnectionFactory.setUsername(username);
        cachingConnectionFactory.setPassword(password);
        return cachingConnectionFactory;
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }
}