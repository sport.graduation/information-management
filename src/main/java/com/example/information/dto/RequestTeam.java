package com.example.information.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RequestTeam implements Serializable {
    private long teamID;
    private long startTimeMillis;
}
