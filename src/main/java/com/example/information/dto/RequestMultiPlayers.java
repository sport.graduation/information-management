package com.example.information.dto;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RequestMultiPlayers implements Serializable {
    private long startTimeMillis;
    private List<Long> playerID;
}
