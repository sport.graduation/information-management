package com.example.information.services;

import com.example.information.dto.RequestForAnalyze;
import com.example.information.dto.RequestMultiPlayers;
import com.example.information.dto.RequestOnePlayer;
import com.example.information.dto.RequestTeam;
import com.example.information.model.daily.DailyData;
import com.example.information.model.daily.DailyDataRepository;
import com.example.information.model.parcticetime.PracticeData;
import com.example.information.model.parcticetime.PracticeDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchService {

    @Autowired
    PracticeDataRepository practiceDataRepository;
    @Autowired
    DailyDataRepository dailyDataRepository;

    public PracticeData playerPracticeTimeData(RequestOnePlayer requestOnePlayer) {
        return practiceDataRepository.findByPlayerIDAndStartTimeMillis(requestOnePlayer.getPlayerID(), requestOnePlayer.getStartTimeMillis());
    }

    public DailyData playerDailyData(RequestOnePlayer requestOnePlayer) {
        return dailyDataRepository.findByPlayerIDAndStartTimeMillis(requestOnePlayer.getPlayerID(), requestOnePlayer.getStartTimeMillis());
    }

    public List<PracticeData> listPlayersPracticeTimeData(RequestMultiPlayers request) {
        return practiceDataRepository.findByStartTimeMillisAndPlayerIDIn(request.getStartTimeMillis(),request.getPlayerID());
    }

    public List<DailyData> listPlayersDailyData(RequestMultiPlayers request) {
        return dailyDataRepository.findByStartTimeMillisAndPlayerIDIn(request.getStartTimeMillis(),request.getPlayerID());
    }

    public List<PracticeData> playersPracticeByTeamID(RequestTeam requestTeam){
        return practiceDataRepository.findAllByTeamIDAndStartTimeMillis(requestTeam.getTeamID(), requestTeam.getStartTimeMillis());
    }

    public List<DailyData> playersDailyByTeamID(RequestTeam requestTeam){
        return dailyDataRepository.findAllByTeamIDAndStartTimeMillis(requestTeam.getTeamID(), requestTeam.getStartTimeMillis());
    }
    public List<PracticeData> returnPracticeDataDuringTime(RequestForAnalyze requestForAnalyze) {
        return practiceDataRepository.findByPlayerIDAndStartTimeMillisGreaterThanEqualAndEndTimeMillisLessThanEqual(
                requestForAnalyze.getPlayerID(), requestForAnalyze.getStartTimeMillis(), requestForAnalyze.getEndTimeMillis());
    }

    public PracticeData returnLastData(long playerID) {
        return practiceDataRepository.findTopByOrderByPlayerIDDesc();
    }

}
