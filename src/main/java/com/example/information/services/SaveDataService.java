package com.example.information.services;

import com.example.information.model.daily.DailyData;
import com.example.information.model.daily.DailyDataRepository;
import com.example.information.model.parcticetime.PracticeData;
import com.example.information.model.parcticetime.PracticeDataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaveDataService implements RabbitListenerConfigurer {

    @Autowired
    PracticeDataRepository practiceDataRepository;
    @Autowired
    DailyDataRepository dailyDataRepository;

    private static final Logger logger = LoggerFactory.getLogger(SaveDataService.class);

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {
    }

    @RabbitListener(queues = "${practicedata.rabbitmq.queue}")
    public void receivedMessage(PracticeData practiceData) {
        logger.info("Practice Data Received, Data : " + practiceData);
        practiceDataRepository.save(practiceData);
    }

    @RabbitListener(queues = "${dailydata.rabbitmq.queue}")
    public void saveDailyData(DailyData dailyData) {
        logger.info("Daily Data Received, Data : " + dailyData);
        dailyDataRepository.save(dailyData);
    }


}
