package com.example.information;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@EnableRabbit
//@EnableDiscoveryClient
@SpringBootApplication
@EnableEurekaClient

public class InformationManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(InformationManagementApplication.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

/*    @Bean
    public Sampler samplerOb() {
        return Sampler.ALWAYS_SAMPLE;
    }*/
}
