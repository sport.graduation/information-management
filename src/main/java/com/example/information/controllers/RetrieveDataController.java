package com.example.information.controllers;

import com.example.information.dto.RequestForAnalyze;
import com.example.information.dto.RequestMultiPlayers;
import com.example.information.dto.RequestOnePlayer;
import com.example.information.dto.RequestTeam;
import com.example.information.model.daily.DailyData;
import com.example.information.model.parcticetime.PracticeData;
import com.example.information.services.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/info")
public class RetrieveDataController {

    @Autowired
    SearchService searchService;

    @PostMapping("/practice/one")
    public ResponseEntity<PracticeData> returnPlayerPracticeData(@RequestBody RequestOnePlayer requestOnePlayer){
        PracticeData data = searchService.playerPracticeTimeData(requestOnePlayer);
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }
    @PostMapping("/daily/one")
    public ResponseEntity<DailyData> returnPlayerDailyData(@RequestBody RequestOnePlayer requestOnePlayer){
        DailyData data = searchService.playerDailyData(requestOnePlayer);
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }

    @PostMapping("/practice/multiplayer")
    public ResponseEntity<List<PracticeData>> returnMultiplePlayerPracticeData(@RequestBody RequestMultiPlayers request){
        List<PracticeData> data = searchService.listPlayersPracticeTimeData(request);
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }

    @PostMapping("/daily/multiplayer")
    public ResponseEntity<List<DailyData>> returnMultiplePlayerDailyData(@RequestBody RequestMultiPlayers request){
        List<DailyData> data = searchService.listPlayersDailyData(request);
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }

    @PostMapping("/practice/team")
    public ResponseEntity<List<PracticeData>> returnTeamPracticeData(@RequestBody RequestTeam requestTeam){
        List<PracticeData> data = searchService.playersPracticeByTeamID(requestTeam);
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }

    @PostMapping("/daily/team")
    public ResponseEntity<List<DailyData>> returnTeamDailyData(@RequestBody RequestTeam requestTeam){
        List<DailyData> data = searchService.playersDailyByTeamID(requestTeam);
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }

    @PostMapping("/practice/one/toAnalyze")
    public ResponseEntity<List<PracticeData>> returnPlayerPracticeDataToAnalyze(@RequestBody RequestForAnalyze requestForAnalyze){
        List<PracticeData> data = searchService.returnPracticeDataDuringTime(requestForAnalyze);
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }

    @GetMapping(value = "/get/{id}")
    public ResponseEntity<PracticeData> getPracticeData(@PathVariable("id") long playerID){
        PracticeData practiceData  = searchService.returnLastData(playerID);
        return ResponseEntity.status(HttpStatus.OK).body(practiceData);
    }

}
