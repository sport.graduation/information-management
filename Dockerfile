FROM openjdk:11.0.16-oracle

WORKDIR /app

EXPOSE 9200

COPY target/*.jar information.jar

ENTRYPOINT [ "java","-jar","./information.jar"]